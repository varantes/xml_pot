package xmlpot;

import br.com.galgo.xmlutils.NamespaceUtils;
import br.com.galgo.xmlutils.XmlUtils;
import com.galgo.utils.xml.SchemaValidator;
import com.galgo.utils.xml.XMLUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.traversal.TreeWalker;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * Created by valdemar.arantes on 08/12/2014.
 */
public class NamespacePOT_EscolaMain {

    private static final Logger log = LoggerFactory.getLogger(XmlPOTMain.class);

    public static void main(String[] args) {

        try {
            //validateEscolaUsingMySchemaValidatorClass();
            //validate2();
            changeNamespacePOT();
        } catch (Exception e) {
            log.error(null, e);
        }

    }

    private static void changeNamespacePOT() throws Exception {
        // Carregando o XML e alterando o Namespace da tag observacoes
        Document doc = loadXmlFile("/escola.xml");
        log.debug("Incluindo a definição de um namespace no elemento observacoes...");
        String obsNamespaceURI = "xsd://schema.observacoes";
        Node obsNode = doc.getElementsByTagName("observacoes").item(0);
        NamespaceUtils.changeTreeNamespace((Element) obsNode, obsNamespaceURI, "");
        final TreeWalker docWalker = getTreeWalker(doc.getDocumentElement());
        printElements(docWalker);
        log.debug("XML depois dos namespaces alterados:\n{}", XmlUtils.toString(doc, true));

        // Validando o XML com o namespace alterado
        validate3(doc);
    }

    private static void validate3(Document doc) throws Exception {
        SchemaFactory schFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        // load a WXS schema, represented by a Schema instance
        final URL escolaXsdURL = NamespacePOT_EscolaMain.class.getResource("/escola.xsd");
        final StreamSource escolaSource = new StreamSource(escolaXsdURL.toExternalForm());

        final URL obsXsdURL = NamespacePOT_EscolaMain.class.getResource("/observacoes.xsd");
        final StreamSource obsSource = new StreamSource(obsXsdURL.toExternalForm());

        Schema schema = schFactory.newSchema(new Source[]{escolaSource, obsSource});

        // create a Validator instance, which can be used to validate an instance document
        Validator validator = schema.newValidator();
        final SAXSource saxSource = new SAXSource(new InputSource(
                NamespacePOT_EscolaMain.class.getResourceAsStream("/escola.xml")));

        validate(doc, validator);

/*
        Node observacoes = doc.getElementsByTagName("observacoes").item(0);
        log.debug("Node observacoes original:\n{}", XmlUtils.toString(observacoes, true));

        validate(observacoes, validator);

        NamespaceUtils.changeElementNamespace((Element) observacoes, "xsd://schema.observacoes", "");
        //NamespaceUtils.changeTreeNamespace(observacoes, "xsd://schema.observacoes", "");
        observacoes = doc.getElementsByTagName("observacoes").item(0);
        log.debug("Depois de alterado o namespace:\n{}", XmlUtils.toString(observacoes, true));

        validate(observacoes, validator);
*/
    }

    private static void validate2() throws Exception {
        Document doc = loadXmlFile("/escola.xml");

        SchemaFactory schFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        // load a WXS schema, represented by a Schema instance
        final URL escolaXsdURL = NamespacePOT_EscolaMain.class.getResource("/escola.xsd");
        final StreamSource escolaSource = new StreamSource(escolaXsdURL.toExternalForm());

        final URL obsXsdURL = NamespacePOT_EscolaMain.class.getResource("/observacoes.xsd");
        final StreamSource obsSource = new StreamSource(obsXsdURL.toExternalForm());

        Schema schema = schFactory.newSchema(new Source[]{escolaSource, obsSource});

        // create a Validator instance, which can be used to validate an instance document
        Validator validator = schema.newValidator();
        final SAXSource saxSource = new SAXSource(new InputSource(
                NamespacePOT_EscolaMain.class.getResourceAsStream("/escola.xml")));

        validate(doc, validator);

        Node observacoes = doc.getElementsByTagName("observacoes").item(0);
        log.debug("Node observacoes original:\n{}", XmlUtils.toString(observacoes, true));

        validate(observacoes, validator);

        NamespaceUtils.changeElementNamespace((Element) observacoes, "xsd://schema.observacoes", "");
        //NamespaceUtils.changeTreeNamespace(observacoes, "xsd://schema.observacoes", "");
        observacoes = doc.getElementsByTagName("observacoes").item(0);
        log.debug("Depois de alterado o namespace:\n{}", XmlUtils.toString(observacoes, true));

        validate(observacoes, validator);
    }

    private static void validate(Node node, Validator validator) throws SAXException, IOException {
        log.debug("Validando {}", node);
        try {
            validator.validate(new DOMSource(node));
        } catch (SAXParseException e) {
            log.error(e.getMessage());
        }
        log.debug("doc validado");
    }

    private static void validateEscolaUsingMySchemaValidatorClass() throws Exception {
/*
        final DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        final DocumentBuilder builder = builderFactory.newDocumentBuilder();
        final Document doc = builder.parse(NamespacePOT_EscolaMain.class.getResourceAsStream("/escola.xml"));

        SchemaFactory schFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        // load a WXS schema, represented by a Schema instance
        Source schemaFile = new StreamSource(new File("target/classes/escola.xsd"));
        Schema schema = schFactory.newSchema(schemaFile);

        // create a Validator instance, which can be used to validate an instance document
        Validator validator = schema.newValidator();
        validator.validate(new DOMSource(doc));
*/

        final File xmlFile = new File(NamespacePOT_EscolaMain.class.getResource("/escola.xml").toURI());
        final URL xsdURL = NamespacePOT_EscolaMain.class.getResource("/escola.xsd");
        final List<String> errors = SchemaValidator.validate(xmlFile, xsdURL);
        if (errors == null || errors.isEmpty()) {
            log.info("Lista de erros vazia");
        } else {
            int counter = 0;
            for (String error : errors) {
                log.debug("erro {}: {}", counter, error);
                counter++;
            }
        }
    }

    private static void changeTreeNamespace(TreeWalker walker, String namespaceURI) {
        NamespaceUtils.changeTreeNamespace(walker, namespaceURI);
    }

    private static TreeWalker getTreeWalker(Node root) {
        return XmlUtils.getTreeWalker(root);
    }

    private static final void printElements(TreeWalker walker) {
        log.debug("Lista dos elementos:\n{}", XmlUtils.printElements(walker));
    }

    private static final Document loadXmlFile(String filename) throws Exception {
        log.debug("Carregando o XML {}", filename);
        Document doc = XMLUtils.buildXml(NamespacePOT_EscolaMain.class.getResource(filename));
        log.debug("XML carregado:\n{}", XmlUtils.toString(doc, true));
        return doc;
    }


}
