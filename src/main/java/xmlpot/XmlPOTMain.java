package xmlpot;

import br.com.galgo.xmlutils.XmlUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * Created by valdemar.arantes on 02/12/2014.
 */
public class XmlPOTMain {
    private static final Logger log = LoggerFactory.getLogger(XmlPOTMain.class);
    private static final String XMLNAMESPACE = "xmlns";

    public static void main(String... args) {

        log.debug("Início");

        // Parse do arquivo XML
        Document dom;
        try {
            dom = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse("xml_test.xml");
        } catch (SAXException | IOException | ParserConfigurationException e) {
            log.error(null, e);
            return;
        }

        // Print do DOM
        log.debug("\n{}", XmlUtils.toString(dom));

        NodeList nodeList = dom.getElementsByTagName("alunos");
        log.info("#tag_alunos={}", nodeList.getLength());

        // Alterando o namespace das tags alunos e printando o resultado
        while (nodeList.getLength() != 0) {
            final Node item = nodeList.item(0);
            changeElementNamespace((Element) item, "namespace.de.teste", "nt");
        }

        log.debug(XmlUtils.toString(dom));
    }

    private static void changeElementNamespace(Element elem, String namespace, String prefix) {
        log.debug("Alterando a tag");
        log.debug(XmlUtils.toString(elem));
        final Node parentNode = elem.getParentNode();

        // Criando um novo elemento com o novo namespace e prefixo
        Element newElement = parentNode.getOwnerDocument().createElementNS(namespace, elem.getNodeName());
        newElement.setPrefix(prefix);

        // Copiando todos os filhos do elemento original para o novo elemento criado acima
        NodeList list = elem.getChildNodes();
        while(list.getLength()!=0) {
            newElement.appendChild(list.item(0));
        }

        // Substituindo o elemento original pelo novo
        parentNode.replaceChild(newElement, elem);
        log.debug("Tag alterada com sucesso:");
        log.debug(XmlUtils.toString(parentNode));
    }

    private static String setNamespace(Element elem, String namespace, String prefix) {
        String pre = getPrefixRecursive(elem, namespace);
        if (pre != null) {
            return pre;
        }
        elem.setAttributeNS(XMLConstants.XMLNS_ATTRIBUTE_NS_URI, "xmlns:" + prefix, namespace);
        return prefix;
    }

    private static String getPrefixRecursive(Element el, String ns) {
        String prefix = getPrefix(el, ns);
        if (prefix == null && el.getParentNode() instanceof Element) {
            prefix = getPrefixRecursive((Element) el.getParentNode(), ns);
        }
        return prefix;
    }

    private static String getPrefix(Element el, String ns) {
        NamedNodeMap atts = el.getAttributes();
        for (int i = 0; i < atts.getLength(); i++) {
            Node node = atts.item(i);
            String name = node.getNodeName();
            if (ns.equals(node.getNodeValue())
                    && (name != null && (XMLNAMESPACE.equals(name) || name.startsWith(XMLNAMESPACE + ":")))) {
                return node.getPrefix();
            }
        }
        return null;
    }
}
