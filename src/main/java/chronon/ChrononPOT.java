package chronon;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by valdemar.arantes on 11/12/2014.
 */
public class ChrononPOT {
    public static final int SIZE = 1000;
    public static final Random GENERATOR = new Random();

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Iniciando...");
        final int[] array = new int[SIZE];
        for (int i = 0; i < SIZE; i++) {
            array[i] = GENERATOR.nextInt();
        }
        final Thread quickSortThread = new Thread(new Runnable() {
            @Override
            public void run() {
                QuickSort.sort(Arrays.copyOf(array, array.length));
            }
        }, "Quick sort");

        final Thread bubbleThread = new Thread(new Runnable() {
            @Override
            public void run() {
                BubbleSort.sort(Arrays.copyOf(array, array.length));
            }
        }, "Bubble sort");

        quickSortThread.start();
        bubbleThread.start();

        quickSortThread.join();
        bubbleThread.join();
        System.out.println("Fim");
    }
}
