package neto.jwebunitpoit;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.ScriptException;
import net.sourceforge.jwebunit.junit.JWebUnit;
import net.sourceforge.jwebunit.util.TestingEngineRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Valdemar on 03/01/2015.
 */
public class JWebUnitPOTMain {

    public static final Logger log = LoggerFactory.getLogger(JWebUnitPOTMain.class);

    public static void main(String[] args) {
        log.debug("Início");
        try {
            JWebUnit.setTestingEngineKey(TestingEngineRegistry.TESTING_ENGINE_HTMLUNIT);
            JWebUnit.setBaseUrl("http://www.uol.com.br");
            JWebUnit.beginAt("/");
        } catch (FailingHttpStatusCodeException e) {
            log.error(e.toString());
        } catch (ScriptException e) {
            log.error(e.toString());
        } catch (Exception e) {
            log.error(null, e);
        }
    }
}
