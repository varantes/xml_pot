package neto.lambdapot;

import com.example.lambda.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by Valdemar on 17/02/2015.
 */
public class ComparatorsPOT {

    public static final Logger log = LoggerFactory.getLogger(ComparatorsPOT.class);

    public static void main(String[] args) {
        log.info("Início");
        List<Person> personList = Person.createShortList();
        log.debug("******************** Antes de ordenar...");
        personList.stream().forEach(p -> System.out.println(p.getSurName()));
        log.debug("******************** Depois de ordenar...");
        personList.stream().sorted(Comparator.comparing(Person::getSurName)).forEach(p -> System.out.println(p.getSurName()));
    }
}
