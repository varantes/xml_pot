package neto.lambdapot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Valdemar on 31/01/2015.
 */
public class LambdaPotMain {
    public static final Logger log = LoggerFactory.getLogger(LambdaPotMain.class);
    public static void main(String[] args) {
        log.info("Início");
        InterfaceWithOneMethod ion = (s) -> "oi, " + s;
        log.info(ion.oneMethod("neto"));
    }


}
