package neto.lambdapot;

/**
 * Created by Valdemar on 31/01/2015.
 */
public interface InterfaceWithOneMethod {
    String oneMethod(String str);
}
